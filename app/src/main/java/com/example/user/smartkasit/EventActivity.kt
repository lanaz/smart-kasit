package com.example.user.smartkasit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_event.*

class EventActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        getExtra()
    }

    fun getExtra() {
        var titleT = intent.getStringExtra("Title")
        var descT = intent.getStringExtra("Desc")
        var imgI = intent.getIntExtra("Image",0 )

        eventTVTitle.text = titleT
        eventTVDesc.text = descT
        eventImgView.setImageResource(imgI)
    }
}
