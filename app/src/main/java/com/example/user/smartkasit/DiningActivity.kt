package com.example.user.smartkasit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dining.*

class DiningActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dining)

        getExtra()
    }

    fun getExtra() {
        var titleT = intent.getStringExtra("Title")
        var descT = intent.getStringExtra("Desc")
        var imgI = intent.getIntExtra("Image",0)

        mTVTitle.text = titleT
        mTVDesc.text = descT
        mImageView.setImageResource(imgI)
    }
}
