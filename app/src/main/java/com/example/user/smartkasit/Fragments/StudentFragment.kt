package com.example.user.smartkasit.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.user.smartkasit.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.fragment_student.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class StudentFragment : Fragment() {
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student, container, false)


//        mAuth.createUserWithEmailAndPassword().

        mButton1Login.setOnClickListener {
            if (validation()){
                var studentID = mEditText1ID.text.toString()
                var studentPassword = mEditText1Password.text.toString()
            }
        }

    }

    fun validation(): Boolean{
        var studentID = mEditText1ID.text.toString()
        var studentPassword = mEditText1Password.text.toString()

        var valid: Boolean = true

        if (studentID.isEmpty()){
            mEditText1ID.error = "Please enter your ID"
            valid = false
        }

        if (studentPassword.isEmpty()){
            mEditText1Password.error = "Please enter your password"
            valid = false
        }
        return valid
    }
}
