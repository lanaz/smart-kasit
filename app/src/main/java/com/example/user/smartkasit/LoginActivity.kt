package com.example.user.smartkasit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.user.smartkasit.Fragments.DriverFragment
import com.example.user.smartkasit.Fragments.InstructorFragment
import com.example.user.smartkasit.Fragments.StudentFragment
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val fragment = ArrayList<Fragment>()
        fragment.add(StudentFragment())
        fragment.add(InstructorFragment())
        fragment.add(DriverFragment())


        var fragmentPagerAdapter = TabAdapter(supportFragmentManager, fragment)
        mViewPager.adapter = fragmentPagerAdapter

        mTabLayout.setupWithViewPager(mViewPager)
    }
}
