package com.example.user.smartkasit.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.user.smartkasit.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_driver.*
import kotlinx.android.synthetic.main.fragment_student.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DriverFragment : Fragment() {
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_driver, container, false)

        mButton3Login.setOnClickListener {
            if (validation()){
                var driverID = mEditText3ID.text.toString()
                var driverPassword = mEditText3Password.text.toString()
            }
        }
    }

    fun validation(): Boolean{
        var driverID = mEditText3ID.text.toString()
        var driverPassword = mEditText3Password.text.toString()

        var valid: Boolean = true

        if (driverID.isEmpty()){
            mEditText3ID.error = "Please enter your ID"
            valid = false
        }

        if (driverPassword.isEmpty()){
            mEditText3Password.error = "Please enter your password"
            valid = false
        }
        return valid
    }
}
