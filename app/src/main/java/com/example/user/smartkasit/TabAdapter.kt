package com.example.user.smartkasit

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class TabAdapter(fm: FragmentManager?, var fragments: ArrayList<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(p0: Int): Fragment {
        return fragments[p0]

    }

    override fun getCount(): Int {
        return fragments.size

    }

    override fun getPageTitle(position: Int): CharSequence? {

        return when (position) {
            0 ->
                "Student"
            1 ->
                "Instructor"
            2 ->
                "Driver"
            else ->
                "Driver"

        }
    }
}