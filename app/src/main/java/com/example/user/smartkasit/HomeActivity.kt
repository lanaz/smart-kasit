package com.example.user.smartkasit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mButtonBus.setOnClickListener {
            var intentBus = Intent(this, BusActivity::class.java)
            startActivity(intentBus)
        }

        mButtonMapJU.setOnClickListener {
            var intentMap = Intent(this, MapJUActivity::class.java)
            startActivity(intentMap)
        }

        mButtonVirtualTour.setOnClickListener {
            var intentVirtual = Intent(this, VirtualTourActivity::class.java)
            startActivity(intentVirtual)
        }

        mButtonLogin.setOnClickListener {
            var intentLogin = Intent(this, LoginActivity::class.java)
            startActivity(intentLogin)
        }

        mButtonDining.setOnClickListener {
            var intentDining = Intent(this, DiningActivity::class.java)
            startActivity(intentDining)
        }

        mButtonEvents.setOnClickListener {
            var intentEvents = Intent(this, EventActivity::class.java)
            startActivity(intentEvents)
        }
    }
}
