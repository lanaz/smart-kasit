package com.example.user.smartkasit

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_dining_list.*

class DiningListActivity : AppCompatActivity() {
   // var list = mutableListOf<Model>()
    var list = mutableListOf<Model>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dining_list)

        list.add(Model("Science Village", "Offers a small variaty of snacks", R.drawable.ic_local_dining_black_24dp))
        list.add(Model("Humanetarians Village", "Offers a small variaty of snacks", R.drawable.ic_local_dining_black_24dp))
        list.add(Model("Milkbar", "Offers breakfast food, lunch like pizza...etc, and snacks", R.drawable.ic_local_dining_black_24dp))
        list.add(Model("UJ Restaurant", "Whole meals for breakfast or lunch", R.drawable.ic_local_dining_black_24dp))
        list.add(Model("UJ Express", "snacks, mua'janat, smoothies, cocktails....", R.drawable.ic_local_dining_black_24dp))

        mListView.adapter = MyListAdapter(applicationContext, R.layout.raw_list_view, list)

        mListView.setOnItemClickListener { parent, view, position, id ->

            if (position == 0) {
                goToDineAct(0)
            }

            if (position == 1) {
                goToDineAct(1)
            }

            if (position == 2) {
                goToDineAct(2)
            }

            if (position == 3) {
                goToDineAct(3)
            }

            if (position == 4) {
                goToDineAct(0)
            }

            if (position == 5) {
                goToDineAct(5)
            }

        }
    }

    fun goToDineAct(position: Int) {
        var title: String = list[position].title
        var desc: String = list[position].description
        var img: Int = list[position].photo
        var intent = Intent(applicationContext, DiningActivity::class.java)
        intent.putExtra("Title", title)
        intent.putExtra("Desc", desc)
        intent.putExtra("Image", img)
        startActivity(intent)
    }
}
