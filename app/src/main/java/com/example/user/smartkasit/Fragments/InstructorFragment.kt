package com.example.user.smartkasit.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.user.smartkasit.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_instructor.*
import kotlinx.android.synthetic.main.fragment_student.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class InstructorFragment : Fragment() {
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_instructor, container, false)

        mButton2Login.setOnClickListener {
            if (validation()){
                var instructorID = mEditText2ID.text.toString()
                var instructorPassword = mEditText2Password.text.toString()
                var instructorPhone = mEditTextPhoneNo.text.toString()
                var instructorVerifyCode = mEditTextVerifyCode.text.toString()
            }
        }
    }

    fun validation(): Boolean{
        var instructorID = mEditText2ID.text.toString()
        var instructorPassword = mEditText2Password.text.toString()
        var instructorPhone = mEditTextPhoneNo.text.toString()
        var instructorVerifyCode = mEditTextVerifyCode.text.toString()

        var valid: Boolean = true

        if (instructorID.isEmpty()){
            mEditText2ID.error = "Please enter your ID"
            valid = false
        }

        if (instructorPassword.isEmpty()){
            mEditText2Password.error = "Please enter your password"
            valid = false
        }

        if (instructorPhone.isEmpty()){
            mEditTextPhoneNo.error = "Please enter your Phone Number"
            valid = false
        }

        if (instructorVerifyCode.isEmpty()){
            mEditTextVerifyCode.error = "Please enter your Verify Code"
            valid = false
        }
        return valid
    }
}
